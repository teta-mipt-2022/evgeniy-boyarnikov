package com.example.demo.services;

import com.example.demo.database.DataBase;
import com.example.demo.database.DataBaseRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Objects.requireNonNullElse;

@Service
public class CoursesFilterService {
    @Autowired
    DataBase dataBase;
    public List<DataBaseRecord> titlePrefixFilter(String prefix){
        return dataBase.filterByTitlePrefix(requireNonNullElse(prefix, ""));
    }
}
