package com.example.demo.services;

import com.example.demo.database.DataBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CoursesExceptionService {
    @Autowired
    DataBase dataBase;

    public boolean isCorrectRequest(String id){
        return dataBase.haveRecord(id);
    }
}
