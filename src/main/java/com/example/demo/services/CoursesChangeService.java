package com.example.demo.services;

import com.example.demo.database.DataBase;
import com.example.demo.exceptions.InvalidFieldException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static java.util.Objects.requireNonNullElse;

@Service
public class CoursesChangeService {
    @Autowired
    DataBase dataBase;

    public void changeCourse(String id, String newAuthor, String newTitle) throws InvalidFieldException {
        if (newAuthor != null){
            if (newAuthor != ""){ dataBase.changeRecordAuthor(id, newAuthor); }
            else { throw new InvalidFieldException("Не корректное поле автора!"); }
        }
        if (newTitle != null){
            if (newTitle != ""){ dataBase.changeRecordTitle(id, newTitle); }
            else{ throw new InvalidFieldException("Не корректное поле заголовка!"); }
        }
    }
}
