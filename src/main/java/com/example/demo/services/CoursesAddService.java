package com.example.demo.services;

import com.example.demo.database.DataBase;
import com.example.demo.exceptions.InvalidFieldException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CoursesAddService {
    @Autowired
    DataBase dataBase;

    public Long addCourse(String author, String title) throws InvalidFieldException {
        if (author == null || author == ""){ throw new InvalidFieldException("Не корректное поле автора!"); }
        if (title == null || title == ""){ throw new InvalidFieldException("Не корректное поле заголовка!"); }
        return dataBase.addRecord(author, title);
    }
}
