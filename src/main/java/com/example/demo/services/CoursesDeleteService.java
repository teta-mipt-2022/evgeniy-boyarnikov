package com.example.demo.services;

import com.example.demo.database.DataBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CoursesDeleteService {
    @Autowired
    DataBase dataBase;

    public void deleteById(String id){
        dataBase.removeRecord(id);
    }
}
