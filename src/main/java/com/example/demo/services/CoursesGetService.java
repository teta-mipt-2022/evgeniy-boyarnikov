package com.example.demo.services;

import com.example.demo.database.DataBase;
import com.example.demo.database.DataBaseRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Objects.requireNonNullElse;

@Service
public class CoursesGetService {
    @Autowired
    DataBase dataBase;

    public DataBaseRecord getCourseById(String id){
        return dataBase.getById(id);
    }
}
