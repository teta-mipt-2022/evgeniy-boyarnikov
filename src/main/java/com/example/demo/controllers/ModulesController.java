package com.example.demo.controllers;

import com.example.demo.postgres.Module;
import com.example.demo.postgres.ModulesRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.List;

@RestController
@RequestMapping("/modules")
@RequiredArgsConstructor
public class ModulesController {

    @Autowired
    ModulesRepository modulesRepository;

    @GetMapping("/all")
    public List<Module> findAll() {
        return modulesRepository.findAll();
    }

    @GetMapping("/{user_id}")
    public List<Module> findByUserId(@PathVariable("user_id") Long id) {
        return modulesRepository.findByAuthorId(id);
    }

    @GetMapping("/time")
    public List<Module> findByCreateTime(@RequestParam(name = "timeFrom", required = true) Long timeFrom, @RequestParam(name = "timeTo", required = true) Long timeTo) {
        return modulesRepository.findByCreateDateGreaterThanEqualAndCreateDateLessThanEqual(new Date(timeFrom), new Date(timeTo));
    }
}
