package com.example.demo.controllers;

import com.example.demo.database.DataBase;
import com.example.demo.database.DataBaseRecord;
import com.example.demo.dto.CourseRequestToUpdate;
import com.example.demo.exceptions.CourseNotFound;
import com.example.demo.exceptions.InvalidField;
import com.example.demo.exceptions.InvalidFieldException;
import com.example.demo.services.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.NoSuchElementException;

import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;
import static java.time.format.DateTimeFormatter.ofLocalizedDate;
import static java.util.Objects.requireNonNullElse;

@RestController
@RequestMapping("/courses")
@RequiredArgsConstructor
public class CoursesController {
    private final CoursesAddService coursesAddService;
    private final CoursesChangeService coursesChangeService;
    private final CoursesDeleteService coursesDeleteService;
    private final CoursesExceptionService coursesExceptionService;
    private final CoursesFilterService coursesFilterService;
    private final CoursesGetService coursesGetService;


    @GetMapping("")
    public String main() {
        return "Hello World!";
    }

    @GetMapping("/{id}")
    public DataBaseRecord getCourse(@PathVariable("id") String id) {
        if (coursesExceptionService.isCorrectRequest(id)) {
            return coursesGetService.getCourseById(id);
        }
        throw new NoSuchElementException("Не найден курс с id: " + id + "!");
    }

    @DeleteMapping("/delete")
    public String deleteCourse(@RequestParam(name = "id", required = true) String id) {
        if (coursesExceptionService.isCorrectRequest(id)) {
            coursesDeleteService.deleteById(id);
            return "Курс Успешно удалён!";
        }
        throw new NoSuchElementException("Не найден курс с id: " + id + "!");
    }

    @PutMapping("/change")
    public String changeCourse(@RequestBody CourseRequestToUpdate request) throws InvalidFieldException {
        if (coursesExceptionService.isCorrectRequest(request.getId())) {
            try {
                coursesChangeService.changeCourse(request.getId(), request.getAuthor(), request.getTitle());
            }catch (InvalidFieldException ex){
                throw new InvalidFieldException(ex.getMessage());
            }
            return "Курс успешно изменён!";
        }
        throw new NoSuchElementException("Не найден курс с id: " + request.getId() + "!");
    }

    @GetMapping("/filter")
    public List<DataBaseRecord> filterCourses(@RequestParam(name = "titlePrefix", required = false) String titlePrefix) {
        return coursesFilterService.titlePrefixFilter(titlePrefix);
    }

    @PostMapping("/add")
    public String addCourse(@RequestBody CourseRequestToUpdate request) throws InvalidFieldException {
        try {
            return "Курс добавлен с id: " + coursesAddService.addCourse(request.getAuthor(), request.getTitle()) + " !";
        } catch (InvalidFieldException ex){
            throw new InvalidFieldException(ex.getMessage());
        }
    }

    @ExceptionHandler
    public ResponseEntity<CourseNotFound> noSuchElementExceptionHandler(NoSuchElementException exception) {
        return new ResponseEntity<>(
            new CourseNotFound(OffsetDateTime.now(), exception.getMessage()),
            HttpStatus.NOT_FOUND
        );
    }

    @ExceptionHandler
    public ResponseEntity<InvalidField> invalidFieldValue(InvalidFieldException exception) {
        return new ResponseEntity<>(
                new InvalidField(OffsetDateTime.now(), exception.getMessage()),
                HttpStatus.BAD_REQUEST
        );
    }

}
