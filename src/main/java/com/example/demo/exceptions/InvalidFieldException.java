package com.example.demo.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class InvalidFieldException extends Exception {
    private String message;
}
