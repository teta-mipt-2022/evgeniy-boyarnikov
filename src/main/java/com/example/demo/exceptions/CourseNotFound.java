package com.example.demo.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.OffsetDateTime;

@AllArgsConstructor
@Data
public class CourseNotFound {
    private OffsetDateTime dateOccurred;
    private String message;
}
