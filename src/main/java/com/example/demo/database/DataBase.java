package com.example.demo.database;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

@Component
@Data
public class DataBase {
    private final Map<String, DataBaseRecord> dataBase;
    private final AtomicLong idGenerator = new AtomicLong(0);

    public DataBase(){
        dataBase = new HashMap();
        Collections.synchronizedMap(dataBase);
        dataBase.put("1", new DataBaseRecord("William Shakespeare", "Storytelling in <Romeo and Juliet>"));
        dataBase.put("2", new DataBaseRecord("Agatha Christie", "How to kill character based on <Whodunits>"));
        idGenerator.addAndGet(3);
    }

    public List<DataBaseRecord> filterByTitlePrefix(String prefix){
        List<DataBaseRecord> result = new ArrayList<>();
        for (String key : dataBase.keySet()) {
            if (dataBase.get(key).getTitle().startsWith(prefix)) {
                result.add(dataBase.get(key));
            }
        }
        return result;
    }

    public Long addRecord(String author, String title){
        DataBaseRecord newRecord = new DataBaseRecord(author, title);
        Long value = idGenerator.addAndGet(1);
        dataBase.put(String.valueOf(value), newRecord);
        return value;
    }
    public DataBaseRecord getById(String id){
        return dataBase.get(id);
    }

    public void removeRecord(String id){
        dataBase.remove(id);
    }

    public void changeRecordAuthor(String id, String newAuthor){
        dataBase.get(id).setAuthor(newAuthor);
    }

    public void changeRecordTitle(String id, String newTitle){
        dataBase.get(id).setTitle(newTitle);
    }

    public boolean haveRecord(String id){
        return dataBase.get(id) != null;
    }
}
