package com.example.demo;

import com.example.demo.postgres.ModulesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.*;
import java.util.Properties;

@SpringBootApplication
public class DemoApplication {
	public static void main(String[] args) throws SQLException {
		SpringApplication.run(DemoApplication.class, args);
	}

}
