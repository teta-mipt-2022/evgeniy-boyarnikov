package com.example.demo.postgres;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
@Entity
@Table(name = "users")
public class User {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;

    @Column(name = "nickname")
    private String nickname;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @OneToMany(mappedBy="user", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<Course> courses;

    @OneToMany(mappedBy="user", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<Module> modules;


    public User() {
    }
}