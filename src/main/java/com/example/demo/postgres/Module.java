package com.example.demo.postgres;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Date;

@Data
@AllArgsConstructor
@Entity
@Table(name = "modules")
public class Module {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long moduleId;

    @Column(name = "course_id")
    private Long courseId;

    @Column(name = "author_id")
    private Long authorId;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "create_date")
    private Date createDate;

    @ManyToOne(optional = false)
    @JoinColumn(name="course", nullable=false)
    private Course course;

    @ManyToOne(optional = false)
    @JoinColumn(name="author", nullable=false)
    private User user;

    public Module() {
    }
}