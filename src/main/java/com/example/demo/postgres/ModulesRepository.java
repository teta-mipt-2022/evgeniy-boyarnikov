package com.example.demo.postgres;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface ModulesRepository extends JpaRepository<Module, Long> {
    List<Module> findByAuthorId(Long id);
    List<Module> findByCreateDateGreaterThanEqualAndCreateDateLessThanEqual(Date dateFrom, Date dateTo);
}
