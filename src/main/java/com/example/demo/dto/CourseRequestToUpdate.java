package com.example.demo.dto;

import lombok.Data;

@Data
public class CourseRequestToUpdate {
    private String author;
    private String title;
    private String id;
}
